#pragma once

namespace register_sgx{
namespace restart_rollback {

constexpr size_t REGISTER_SIZE = 8;

} // namespace restart_rollback
} // namespace register_sgx
